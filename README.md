# 🦈 jaws

> A tool to manage AWS resources and run tasks

## Usage

Help

```
jaws --help
```

Usage

```
jaws <SUBCOMMAND>
```

Help with Services

```
jaws dynamodb --help
```

Help with Commands

```
jaws dynamodb create-table --help
```

Services and Commands

- `dynamodb`
